import cv2
import sys 
from copy import deepcopy
import logging as log
import numpy as np
from PIL import Image
import pytesseract
from utils import crop_to_contour


def crop_character(img, dimensions):
    x, y, w, h = dimensions
    character = deepcopy(img[y:y+h,x:x+w])
    return character

def find_characters_countours(plate_img):
    w, h, _ = plate_img.shape 
    if w < 1 or h < 1: return []
    gray = cv2.cvtColor(plate_img.copy(), cv2.COLOR_BGR2GRAY)
    gray = cv2.GaussianBlur(gray, (3,3), 0)
    
    threshold = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)
    h, w = threshold.shape
    
    threshold = threshold[int(h*0.03):int(h*0.9), int(w*0.03):int(w*0.99)]
    _, contours, _ = cv2.findContours(threshold, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    w, h, x, y = 0, 0, 0, 0
    chars = []
    rough_h = gray.shape[0] * 0.7
    log.debug(f'Rough height {rough_h}')
    for contour in contours:
        # rough range of areas of a plate number
        [x,y,w,h] = cv2.boundingRect(contour)

        # rough dimensions of a character
        if h < rough_h:
            continue
        ratio = 20/35 

        if ratio - 0.5 < w/h < ratio + 0.5:
            character = crop_character(threshold, [x,y,w,h])
            chars.append([x, character])
            cv2.rectangle(plate_img, (x,y), (x+w, y+h), (0,0,255), 1)

    log.debug(f'Found {len(chars)} characters')
    chars = sorted(chars, key=lambda x: x[0])
    return chars


def extract_text(img, c):
    cropped = crop_to_contour(img, c)
    chars_images = find_characters_countours(cropped)
    chars = ''
    for char_image in chars_images:
        pil_img = Image.fromarray(char_image[1])
        text = pytesseract.image_to_string(pil_img, config='-psm 10')
        chars += text

    log.debug(f'Found plate [{chars}]')
    return chars

def extract_text_all(img, plate_candidates):
    return [extract_text(img, c) for c in plate_candidates]

def filter_plates(plates):
    return [p for p in plates if len(p) in (7, 8, 9)]