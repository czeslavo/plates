from logger import logger as log
from matplotlib import pyplot as plt
import cv2
from imutils import opencv2matplotlib


class ImagesInspector:
    def __init__(self):
        self._imgs = {}
        log.debug('Images inspector created')

    def register_image(self, img, title='Untitled'):
        self._imgs[title] = img.copy()
        log.debug(f'Registered image to inspect with titlte {title}')

    def inspect(self):
        for title, img in self._imgs.items():
            plt.figure(title)
            plt.imshow(opencv2matplotlib(img) if len(img.shape) == 3 else img)
        plt.show()

images_inspector = ImagesInspector()

if __name__ == '__main__':
    im1 = cv2.imread('./images/1.jpg')
    im2 = cv2.imread('./images/3.jpg')

    images_inspector.register_image(im1, 'image 1')
    images_inspector.register_image(im2, 'image 2')
    images_inspector.inspect()
        