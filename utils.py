import cv2
import numpy as np
import logging as log 

def crop_to_contour(img, rect):

    # rotate img
    angle = rect[2]
    rows,cols = img.shape[0], img.shape[1]
    M = cv2.getRotationMatrix2D((cols/2,rows/2),angle,1)
    img_rot = cv2.warpAffine(img,M,(cols,rows))

    # rotate bounding box
    box = cv2.boxPoints(rect)
    pts = np.int0(cv2.transform(np.array([box]), M))[0]    
    pts[pts < 0] = 0

    # crop
    img_crop = img_rot[pts[1][1]:pts[0][1], 
                       pts[1][0]:pts[2][0]]

    return img_crop

def setup_logging(verbose = False):
    log_level = log.DEBUG if verbose else log.INFO
    log.basicConfig(level=log_level)

def load_image(path):
    return cv2.imread(path)


def preview(img, title='untitled'):
    cv2.imshow(title, img)
    cv2.waitKey(0)


def draw_contours(img, candidates):
    for rect in candidates:
        box = cv2.boxPoints(rect)
        box = np.int0(box)
        cv2.drawContours(img, [box], 0, (0, 255, 0), 3)