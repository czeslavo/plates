# Plate detector
Narzędzie do wyszukiwania i odczytywania tablic rejestracyjnych ze zdjęć. 

* wykrywanie tablic na zdjęciu zrealizowano z pomocą biblioteki *OpenCV*
* do odczytywania numerów rejestracyjnych wykorzystano bibliotekę OCR *Tesseract*

# Schemat działania 

![Schemat działania](https://i.imgur.com/v4a9Aqa.png)

# Użycie
Aby zainstalować zależności, należy wywołać komendę:
```
pip install -r requirements.txt
```

Punktem wejściowym jest skrypt `plates.py`, który uruchamiamy z następującymi parametrami:

```
usage: plates.py [-h] [-v] image

Find car plates on your photo

positional arguments:
  image          path to the image you want to analyse

optional arguments:
  -h, --help     show this help message and exit
  -v, --verbose  print more logs to stdout
```

Aby odczytać numer rejstracyjny z tablicy ze zdjęcia w images/1.jpg, należy wywołać komendę:

```
python plates.py ./images/1.jpg 
```

W celu uzyskania większej ilości logów informujących o postępach analizy można dodać do wywołania flagę `-v`.

# Przykładowe wyniki

## Wejściowy obraz:
![Obraz wejściowy](./images/1.jpg)

## Wywołanie programu:
```
PS C:\Users\czesl\Desktop\aipo\plates> python .\plates.py .\images\1.jpg -v
DEBUG:root:Plate candidate found
DEBUG:root:Found 1 plate candidates
DEBUG:root:Rough height 34.3
DEBUG:root:Found 7 characters
DEBUG:root:Found plate [GD4845K]
INFO:root:Found plates: ['GD4845K']
```