import logging as log

def setup_logging(logger, verbose):
    log_level = log.DEBUG if verbose else log.INFO
    logger.basicConfig(level=log_level)

logger = log.getLogger()