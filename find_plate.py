import cv2
import sys 
from copy import deepcopy
import logging as log
import numpy as np
from PIL import Image
import pytesseract

def prepare_image(img, threshold_type = cv2.THRESH_BINARY, block_size = 43):
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img = cv2.medianBlur(img, 5)
    img = cv2.adaptiveThreshold(img, 255, \
                                 cv2.ADAPTIVE_THRESH_GAUSSIAN_C, \
                                 threshold_type, \
                                 block_size, 2)
    return img

def get_contours(img):
    _, contours, _ = cv2.findContours(img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    return contours

def get_candidates(contours):
    candidates = []
    for c in contours:
        area = cv2.contourArea(c)

        if area < 1000: # parametrize it?
            continue 

        rect = cv2.minAreaRect(c)
        _, dim, _ = rect
        w, h = dim
        if h == 0.0: continue

        # Rodzaj tablicy	                                Wymiary
        # Jednorzędowe tablice dla samochodów oraz przyczep	520 x 114 mm 
        if 4 < w / h < 5:
            log.debug('Plate candidate found')
            candidates.append(rect)
    log.debug(f'Found {len(candidates)} plate candidates')
    return candidates
                    