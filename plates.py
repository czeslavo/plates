from logger import logger as log
from logger import setup_logging
import cv2
import sys
from copy import deepcopy
from find_plate import prepare_image, get_contours, get_candidates
from extract_text import extract_text_all, filter_plates
from inspector import images_inspector
from utils import *
import argparse


def parse_args():
    parser = argparse.ArgumentParser(description='Find car plates on your photo')
    parser.add_argument('image', type=str, help='path to the image you want to analyse')
    parser.add_argument('-v', '--verbose', action='store_true', help='print more logs to stdout')
    parser.add_argument('-i', '--inspect', action='store_true', help='show images in progress')
    return parser.parse_args()


def get_plates(img_path):
    img = load_image(img_path)
    images_inspector.register_image(img, 'Input image')
    
    prepared_img = prepare_image(deepcopy(img))
    images_inspector.register_image(prepared_img, 'Image prepared for finding plates\' locations')

    contours = get_contours(prepared_img)
    plate_candidates = get_candidates(contours)
    with_contours = img.copy()
    draw_contours(with_contours, plate_candidates)
    images_inspector.register_image(with_contours, 'Candidates for plates')

    plates_text = extract_text_all(img, plate_candidates)
    plates_text = filter_plates(plates_text) 
    return plates_text


if __name__ == '__main__':
    args = parse_args()
    from logger import setup_logging
    setup_logging(log, args.verbose)

    plates = get_plates(args.image)
    log.info(f'Found plates: {plates}')

    if args.inspect: images_inspector.inspect()
